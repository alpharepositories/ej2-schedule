import * as index from './index';
index.Schedule.Inject(index.Day, index.Week, index.WorkWeek, index.Month, index.Agenda, index.MonthAgenda, index.TimelineViews, index.TimelineMonth, index.Resize, index.DragAndDrop, index.ExcelExport, index.ICalendarExport, index.ICalendarImport);
export * from './index';
